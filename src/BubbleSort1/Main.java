package BubbleSort1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {6,4,3,2};
		int[] result = bubbleSort(arr);
		displayArray(result);
		
	}
	public static int[] bubbleSort(int[] arr) {
		int length = arr.length;
		for(int counter = 0;counter < length-1;counter++) {
			for(int i = 1;i<length;) {
				if(arr[i-1] > arr[i]) {
					int temp = arr[i];
					arr[i] = arr[i-1];
					arr[i-1] = temp;	
				}
				i++;
			}
		}
	
		return arr;
	}
	
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}	

}
